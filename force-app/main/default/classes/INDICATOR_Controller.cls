public class INDICATOR_Controller {
    @AuraEnabled
    public static String getSObjectLabel(String sObjectName){
        String label=Schema.getGlobalDescribe().get(sObjectName).getDescribe().getLabel();
        return label;
    }
}